#ifndef LIBTERMCODE
#define LIBTERMCODE

enum {
	COLMODENONE,
	COLMODE4,
	COLMODE8,
	COLMODE24
};

enum {
	COLOR_BLACK,
	COLOR_RED,
	COLOR_GREEN,
	COLOR_YELLOW,
	COLOR_BLUE,
	COLOR_MAGENTA,
	COLOR_CYAN,
	COLOR_WHITE,
};

#define STYLE_INTENSE  (1<<1)   /* SGR code 1 */
#define STYLE_BRIGHT   (1<<2)   /* set to use 9x and 10x colours instead */
#define STYLE_FAINT    (1<<3)   /* SGR code 2 */
#define STYLE_ITALIC   (1<<4)   /* SGR code 3 */
#define STYLE_ULINE    (1<<5)   /* SGR code 4 */
#define STYLE_REVERS   (1<<6)   /* SGR code 7 */
#define STYLE_RESET    (1<<7)   /* SGR code 0 */
#define STYLE_END      (1<<8)   /* Add '2' in front of above codes when added */
#define STYLE_BLINK    (1<<9)   /* SGR code 5 */
#define STYLE_CONCEAL  (1<<10)  /* SGR code 8 (NB: 'Not widely supported') */

#define CURSOR_BLOCK  1
#define CURSOR_ULINE  2
#define CURSOR_IBEAM  4
#define CURSOR_BLINK  8
#define CURSOR_HIDE  16

union tccolor {
	short r, g, b;
	short code;
};

struct tcstyle {
	union tccolor fg, bg;
	int style;
};

struct tcscreen {
	int width, height;
	int color_mode, has_resized; /* since last refresh */
	struct tcstyle *colbuf;
	char *bufa, *bufb;
	size_t bufsize;
};


void tuishow(void)
{
	printf("\x1b[?1049h");
}

void tuihide(void)
{
	printf("\x1b[?1049l");
}

void tc_curhide(void)
{
	printf("\x1b[?25l");
}

void tc_curshow(void)
{
	printf("\x1b[?25h");
}

void curtype(int opt)
{
	const int block_cursor = 2;
	const int uline_cursor = 4;
	const int ibeam_cursor = 6;
	int cursor = ((opt & CURSOR_BLOCK) ? block_cursor
	           : ((opt & CURSOR_ULINE) ? uline_cursor
		   : ((opt & CURSOR_IBEAM) ? ibeam_cursor
		   : block_cursor))); /* in case no options are passed */
	cursor -= !!(opt & CURSOR_BLINK);

	printf(stdout, "\x1b[%d q", cursor);
}

int tcopen(struct tcscreen *screen, int curopt, int colormode)
{
	if (!screen) { return -1; }
	setvbuf(stdout, NULL, _IONBF, 0);

	if (options & CURSOR_HIDE) { curhide(); }
	else { curtype(curopt); }
}

int tcclose(struct tcscreen *screen)
{
}
#endif
